set tabstop=2
set softtabstop=2
" set listchars=tab:\▏\
set number

highlight ExtraWhitespace ctermbg=darkgreen guibg=lightgreen
match ExtraWhitespace /\s\+$/

" Plugins
call plug#begin('~/.vim/plugged')

Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Plug 'michamos/vim-bepo'

let g:airline_theme='lucius'

call plug#end()

" Set the :Format command
command! -nargs=0 Fmt :call CocAction('format')

" Yanking to +y & Pasting from +p
set clipboard+=unnamedplus
