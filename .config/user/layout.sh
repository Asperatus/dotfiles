#!/bin/bash

read variant <<< $( setxkbmap -query | grep variant | awk '{print $2}' )

if [ "$variant" == "programmer_beop" ]; then
	setxkbmap -layout fr -option caps:escape
else
	setxkbmap -layout fr programmer_beop -option caps:escape
fi
