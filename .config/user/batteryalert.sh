#!/bin/bash

BATTERY=$(upower -e | grep 'BAT')

while :
do
	BATTERY_PERCENTAGE=$(upower -i $BATTERY | grep percentage | awk '{ print $2 }' | sed s/'%'/''/g)
	CABLE=$(upower -d | grep -n2 line-power | grep online | awk '{ print $3 }')

	if [[ "$BATTERY_PERCENTAGE" -le "4" && $CABLE = "no" ]]; then
		/usr/bin/systemctl hibernate
	fi

	if [[ "$BATTERY_PERCENTAGE" -le "10" && $CABLE = "no" ]]; then
		notify-send --urgency=critical "WARNING: Battery is about to die" "Plug in the power cable"
	fi

	if [[ "$BATTERY_PERCENTAGE" -ge "80" && $CABLE = "yes" ]]; then
    notify-send "Battery is sufficiently charged" "You can remove the plug"
	fi

sleep 10

done
