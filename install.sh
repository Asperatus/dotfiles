#!/bin/sh

# WM and X server
sudo pacman -S --needed xorg i3-gaps

# Apps
sudo pacman -S alacritty dunst neovim picom polybar rofi firefox

cp -r homedir/ $(HOME)
cp -r .config/ $(HOME)/.config

printf "\e[91mMake sure you install the proper AMD/Intel/NVIDIA drivers!\e[39m\n"
printf " - AMD: install xf86-video-amdgpu, mesa and lib32-mesa\n"
printf " - ATI: install xf86-video-ati, mesa and lib32-mesa\n"
printf " - Intel: install xf86-video-intel, mesa and lib32-mesa\n"
printf " - NVIDIA: install nvidia, nvidia-utils and lib32-nvidia-utils\n"
